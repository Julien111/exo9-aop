package com.banque.spring.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.banque.entity.IUtilisateurEntity;

/**
 * Supprimer les espaces avec trim.
 */
@Aspect
public class TrimAspect {
	
			@Around("execution(* com.banque.service.AuthentificationService.authentifier(String,String)) && args(param1, param2)")
			public Object cache(ProceedingJoinPoint pj, String param1, String
			param2) throws Throwable {			
			 if (LogAspect.LOG.isInfoEnabled()) {
					LogAspect.LOG.info("Passage autentifier");
				}	
			 Object[] tab = pj.getArgs();
			 
			 tab[0] =  ((String) tab[0]).trim();
			 tab[1] = ((String) tab[1]).trim();
			 
			return pj.proceed(tab);			 
			}
}

